package menu;

import javax.persistence.*;

@Entity
@Table(name="dishes")
public class Dish {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @Column(nullable = false)
    private  String dish;
    @Column(nullable = false)
    private  int mass;
    @Column(nullable = false)
    private  float price;
    @Column
    private  int discount;
    public Dish() {
    }

    public Dish(String dish, int mass, float price) {
        this.dish = dish;
        this.mass = mass;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "{"+
                "id=" + id +
                ", dish='" + dish + '\'' +
                ", mass=" + mass +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }
}
