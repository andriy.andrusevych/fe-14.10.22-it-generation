package jpa1;

import java.util.Scanner;

import static jpa1.Utils.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            while (true) {
                System.out.println("1: add appartment");
                System.out.println("2: delete appartment");
                System.out.println("3: find appartment");
                System.out.print("-> ");

                String s = sc.nextLine();
                switch (s) {
                    case "1":
                        addAppartment();
                        break;
                    case "2":
                        deleteAppartment();
                        break;
                    case "3":
                         findAppartment();
                        break;
                    default:
                        return;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
                sc.close();
            }
    }
}