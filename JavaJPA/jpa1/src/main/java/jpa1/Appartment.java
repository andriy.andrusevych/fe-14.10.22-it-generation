package jpa1;

import javax.persistence.*;

@Entity
@Table(name="appartments")
public class Appartment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @Column(nullable = false)
    private  String district;
    @Column(nullable = false)
    private  String address;
    @Column(nullable = false)
    private  int area;
    @Column(nullable = false)
    private  int roomsNumber;
    @Column
    private  float price;

    public Appartment() {

    }
    public Appartment(String district, String address, int area, int roomsNumber, float price) {
        this.district = district;
        this.address = address;
        this.area = area;
        this.roomsNumber = roomsNumber;
        this.price = price;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getRoomsNumber() {
        return roomsNumber;
    }

    public void setRoomsNumber(int roomsNumber) {
        this.roomsNumber = roomsNumber;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Квартира " +
                "№=" + id +
                ": район='" + district + '\'' +
                ", адреса='" + address + '\'' +
                ", площа=" + area +
                ", к-ть кімнат=" + roomsNumber +
                ", ціна=" + price +
                ';';
    }
}
