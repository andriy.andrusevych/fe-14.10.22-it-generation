package jpa1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Utils {
    static EntityManagerFactory emf;
    static EntityManager em;
    public static void addAppartment() {
        String newDistrcit = "n/a";
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter address: ");
        String newAddress = sc.nextLine();
        System.out.print("""
                Choose district:
                1 for ФРАНКІВСЬКИЙ
                2 for ЗАЛІЗНИЧНИЙ
                3 for СИХІВСЬКИЙ
                4 for ЛИЧАКІВСЬКИЙ
                """);
        String s = sc.nextLine();
        switch (s) {
            case "1":
                newDistrcit = String.valueOf(Districts.ФРАНКІВСЬКИЙ);
                break;
            case "2":
                newDistrcit = String.valueOf(Districts.ЗАЛІЗНИЧНИЙ);
                break;
            case "3":
                newDistrcit = String.valueOf(Districts.СИХІВСЬКИЙ);
                break;
            case "4":
                newDistrcit = String.valueOf(Districts.ЛИЧАКІВСЬКИЙ);
                break;
            default:
                return;
        }
        System.out.print("Enter appartment area: ");
        int newArea = Integer.parseInt(sc.nextLine());
        System.out.print("Enter number of rooms: ");
        int newRooms = Integer.parseInt(sc.nextLine());
        System.out.print("Enter the price: ");
        float newPrice = Float.parseFloat(sc.nextLine());

        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Appartment newAppartment = new Appartment(newDistrcit, newAddress, newArea, newRooms, newPrice);
            em.persist(newAppartment);
            em.getTransaction().commit();

            System.out.println(newAppartment.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }finally {
            em.close();
            emf.close();
        }
    }
    public static void deleteAppartment() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter appartment id: ");
        String appId = sc.nextLine();
        long id = Long.parseLong(appId);
        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        Appartment a = em.getReference(Appartment.class, id);
        if (a == null) {
            System.out.println("Appartment not found!");
            return;
        }
        em.getTransaction().begin();
        try {
            em.remove(a);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    public static void findAppartment(){
        Scanner sc = new Scanner(System.in);
        System.out.print("""
                Specify your search:
                1 - search by price;
                2 - search by district;
                3 - search by number of rooms;
                4 - show all available app's;
                """);
        String criteria = sc.nextLine();
        switch (criteria) {
            case "1":
               searchByPrice();
                break;
            case "2":
                searchByDistrict();
                break;
            case "3":
                searchByRooms();
                break;
            case "4":
                showAllAppartments();
                break;
            default:
                return;
        }
    }
    private static void showAllAppartments() {

        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Appartment a", Appartment.class);
        List<Appartment> list = (List<Appartment>) query.getResultList();

        for (Appartment a : list)
            System.out.println(a);
    }
    private static void searchByPrice() {
        float min = 0; //default
        float max = 1000000;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter min price:");
        String minS = sc.nextLine();
        if (!minS.isEmpty()){
        min = Float.valueOf(minS);}
        System.out.println("Enter max price");
        String maxS = sc.nextLine();
        if(!maxS.isEmpty()){
        max =  Float.valueOf(maxS);}
        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        Query query = em.createQuery("from Appartment a where a.price>:minPrice AND a.price<:maxPrice")
                .setParameter("minPrice", min)
                .setParameter("maxPrice", max);
        List<Appartment> list = (List<Appartment>) query.getResultList();
        if(list.isEmpty()){
            System.out.println("NO appartments found!\n");
        }else{
        System.out.printf("We FOUND the following appartments:\n");
        for (Appartment a : list)
            System.out.println(a);}
    }
    private static void searchByRooms() {
        int min = 1;
        int max = 10;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter min number of rooms:");
        String minS = sc.nextLine();
        if (!minS.isEmpty()){
            min = Integer.valueOf(minS);}
        System.out.println("Enter max number of rooms");
        String maxS = sc.nextLine();
        if(!maxS.isEmpty()){
            max =  Integer.valueOf(maxS);}
        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        Query query = em.createQuery("from Appartment a where a.roomsNumber>:minPrice AND a.roomsNumber<:maxPrice")
                .setParameter("minPrice", min)
                .setParameter("maxPrice", max);
        List<Appartment> list = (List<Appartment>) query.getResultList();
        list.sort(Comparator.comparingInt(Appartment::getRoomsNumber));
        if(list.isEmpty()){
            System.out.println("NO appartments found!\n");
        }else{
            System.out.printf("We FOUND the following appartments:\n");
            for (Appartment a : list)
                System.out.println(a);}
    }
    private static void searchByDistrict() {
        String choiceDistrict = String.valueOf(Districts.СИХІВСЬКИЙ);//default
        Scanner sc = new Scanner(System.in);
        System.out.print("""
                Choose district:
                1 for ФРАНКІВСЬКИЙ
                2 for ЗАЛІЗНИЧНИЙ
                3 for СИХІВСЬКИЙ
                4 for ЛИЧАКІВСЬКИЙ
                """);
        String s = sc.nextLine();
        switch (s) {
            case "1":
                choiceDistrict = String.valueOf(Districts.ФРАНКІВСЬКИЙ);
                break;
            case "2":
                choiceDistrict = String.valueOf(Districts.ЗАЛІЗНИЧНИЙ);
                break;
            case "3":
                choiceDistrict = String.valueOf(Districts.СИХІВСЬКИЙ);
                break;
            case "4":
                choiceDistrict = String.valueOf(Districts.ЛИЧАКІВСЬКИЙ);
                break;
            default:
                showAllAppartments();
        }

        emf = Persistence.createEntityManagerFactory("JPAappartments");
        em = emf.createEntityManager();
        Query query = em.createQuery("from Appartment a where a.district=:choiceDistrict")
                .setParameter("choiceDistrict", choiceDistrict);
        List<Appartment> list = (List<Appartment>) query.getResultList();
        if(list.isEmpty()){
            System.out.println("NO apartments found!\n");
        }else{
            System.out.printf("We FOUND the following apartments:\n");
            for (Appartment a : list)
                System.out.println(a);}
    }
}
