package bank;

import bank.Client;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.Random;

@Entity
@Table
public class Account {

    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "acc_seq")
    @SequenceGenerator(name = "acc_seq", allocationSize = 5, initialValue = 21385000)
    //встановлюємо крок і початкові цифри 21 для IBAN для України + МФО банку)
    @Column(name = "id")
    private long id;

    @Column(nullable = false)
    private long acc_Number;
    @Column(nullable = false)
    private String acc_Currency;
    @Column
    private double balance;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "client_id")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    public Account() {
    }

    public Account(String acc_Currency, Client client) {
        this.acc_Currency = acc_Currency;
        this.acc_Number = new Random().nextLong(1_000, 1_000_000);
        this.balance = 0.00;
    }
    public Account(String acc_Currency) {
        this.acc_Currency = acc_Currency;
        this.acc_Number = new Random().nextLong(1_000, 1_000_000);
        this.balance = 0.00;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAcc_Number() {
        return acc_Number;
    }

    public void setAcc_Number(long acc_Number) {
        this.acc_Number = acc_Number;
    }

    public String getAcc_Currency() {
        return acc_Currency;
    }

    public void setAcc_Currency(String acc_Currency) {
        this.acc_Currency = acc_Currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account account)) return false;
        return id == account.id && acc_Number == account.acc_Number && acc_Currency.equals(account.acc_Currency);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "acc_Number=" + acc_Number +
                ", acc_Currency='" + acc_Currency + '\'' +
                '}';
    }
}

