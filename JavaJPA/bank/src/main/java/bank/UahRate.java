package bank;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table
public class UahRate { //курс гривні до валют

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(nullable = false)
    private double EurRate = 39.00;

    @Column(nullable = false)
    private double USDRate = 40.00;

    @Column(nullable = false)
    private LocalDate date;


    public UahRate() {
        this.date = LocalDate.now();
    }

    public double getEurRate() {
        return EurRate;
    }

    public void setEurRate(double eurRate) {
        EurRate = eurRate;
    }

    public double getUSDRate() {
        return USDRate;
    }

    public void setUSDRate(double USDRate) {
        this.USDRate = USDRate;
    }
}
