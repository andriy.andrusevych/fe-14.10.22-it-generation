package academy.prog;

public class ResultDTO extends DTO {
    protected String type;
    protected int x;
    protected int y;
    protected int result;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    static public int calculate (int x, int y, String type){
      int result;
      switch (type){
          case "sum":
              result = x+y;
              break;
          case "mult":
              result = x*y;
              break;
          default:
              result= Integer.parseInt(null);
              break;
      }
      return result;
    };
}
