package academy.prog;

import org.apache.coyote.Request;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
    private final Service service;

    public Controller(Service service) {
        this.service = service;
    }

    @GetMapping("calc")
    public ResultDTO calc(
            @RequestParam String type,
            @RequestParam int x,
            @RequestParam int y) {
        var requestDTO = new DTO();
        requestDTO.setType(type);
        requestDTO.setX(x);
        requestDTO.setY(y);
        long id = service.saveRequest(requestDTO);
        int calcResult = ResultDTO.calculate(x, y, type);
        var result = new ResultDTO();
        result.setType(requestDTO.getType());
        result.setX(requestDTO.getX());
        result.setY(requestDTO.getY());
        result.setResult(calcResult);
        return result;
    }


}
