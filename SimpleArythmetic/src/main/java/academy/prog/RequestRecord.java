package academy.prog;

import javax.persistence.*;

@Entity
public class RequestRecord {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private int x;
    @Column(nullable = false)
    private int y;
    @Column
    private int result;

    public RequestRecord() {
    }
    public RequestRecord(String operation) {
        this();
        this.type = operation;
    }

    public static RequestRecord of(DTO DTO) {
        return new RequestRecord(DTO.getType());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getX() {
        return x;
    }
    public void setX(Integer param1) {
        this.x = param1;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer param2) {
        this.y = param2;
    }
}
