package academy.prog;

import org.springframework.transaction.annotation.Transactional;


@org.springframework.stereotype.Service
public class Service {
    private final RequestRepository requestRepository;

    public Service(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Transactional
    public long saveRequest(DTO DTO) {
        var RequestRecord = new RequestRecord();
        RequestRecord = academy.prog.RequestRecord.of(DTO);
        requestRepository.save(RequestRecord);
        System.out.println(DTO);
        return RequestRecord.getId();
    }


}
