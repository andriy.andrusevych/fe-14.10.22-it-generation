import React from 'react';
import ReactDOM from 'react-dom';
import data from './data';

function App() {
  let code = '<tr><td>text</td></tr>';
  return (
    <table>
      <thead>
        <tr>
          <th>Код валюти</th>
          <th>Валюта</th>
          <th>Курс</th>
        </tr>
      </thead>
      <tbody>
        {data.map(value => (
          <tr key={value.r030}>
            <td>{value.cc}</td>
            <td>{value.txt}</td>
            <td>{value.rate}</td>

          </tr>
        ))}
      </tbody>
    </table>
  );
}

ReactDOM.render(<App></App>, document.getElementById("exchange"));