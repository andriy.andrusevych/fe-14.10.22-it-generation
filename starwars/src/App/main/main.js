import React from "react";
import { characters } from "../data/characters";
import Card from "../cards/card";
import "./main.css"

function Main() {

    return (
    <div className="cardsMain">
        {
            characters.map((el)=>(
                <Card character={el} key={el.name}></Card>
            ))
        }
    </div>
    )
}
export default Main;