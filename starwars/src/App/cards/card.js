import React from "react";
import "./card.css";

function Card(character) {
    const {name, height, mass, hair_color,  eye_color, birth_year, gender, url}  = character.character;


    return (
    <div className="card">
        <div className="card_title">{name}</div>
        <div className="card_pic"><a href={url} target="_blank"><img src={require("./img/starwars.png")} alt="starwars logo"></img></a></div>
        <div className="card_height"><span>Height: </span>{height}</div>
        <div className="card_mass"><span>Mass: </span>{mass}</div>
        <div className="card_hair_color"><span>Hair Color: </span>{hair_color}</div>
        <div className="eye_color"><span>Eye Color: </span>{eye_color}</div>
        <div className="card_birth_year"><span>Birth Year: </span>{birth_year}</div>
        <div className="gender"><span>Gender: </span>{gender}</div>
        <div className="card_link"><a href={url} target="_blank">Open Character</a></div>
    </div>
    )
}
export default Card;