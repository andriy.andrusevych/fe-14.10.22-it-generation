// плавний перехід по сторінці:
// Привʼязуємо кліки на всіх посиланнях до функції і додаємо анімацію з затримкою 1000мс
$(document).on('click', 'a', function (event) {
  event.preventDefault();
  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 1000);
});

//валідація форми бронювання

let formValidated = false;
//email
const femail = document.getElementById('femail');
const email_label = document.getElementById("femail_label");
const emailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
femail.addEventListener('input', (e) => {
  if (!emailRegex.test(femail.value)) {
    email_label.classList.add('red');
    formValidated = false;
  } else {
    email_label.classList.remove('red');
    formValidated = true;
  };
});

//name - only Engish & Ukrainian letter and white space allowed
const fname = document.getElementById('fname');
const fname_label = document.getElementById('fname_label');
const nameRegex = /^[a-zA-Zа-яА-яїіґʼ\s]+$/;
fname.addEventListener('input', (e) => {
  if (!nameRegex.test(fname.value)) {
    fname_label.classList.add('red');
    formValidated = false;
  } else {
    fname_label.classList.remove('red');
    formValidated = true;
  };
});

//phone - only UA phone 
const fnumber = document.getElementById('fnumber');
const fnumber_label = document.getElementById('fnumber_label');
const numRegex = /^[+380]+[0-9]{12}$/;
fnumber.addEventListener('input', (e) => {
  if (!numRegex.test(fnumber.value)) {
    fnumber_label.classList.add('red');
    formValidated = false;
  } else {
    fnumber_label.classList.remove('red');
    formValidated = true;
  };
});

//putting current date-time into form

function zeroPadded(val) {
  if (val >= 10)
    return val;
  else
    return '0' + val;
}

$(document).ready(function () {
  d = new Date();
  $('input[name="fdatetime"]').val(d.getFullYear() + "-" + zeroPadded(d.getMonth() + 1) + "-" + zeroPadded(d.getDate()) + "T" + zeroPadded(d.getHours()) + ":" + zeroPadded(d.getMinutes()));
});

//saving data to session storage from reservation form

let usedata = [];
function saveData(e) {
  e.preventDefault();
  if (formValidated) {
    const fdatetime = document.getElementById('fdatetime');
    usedata.push(fdatetime.value);
    usedata.push(fname.value);
    usedata.push(femail.value);
    usedata.push(fnumber.value);
    sessionStorage.setItem("reservation", usedata);
  } else {
    alert("Check all fields!")
  }
  return true;
};


//getting all pizzas & prices choices in constructor submenu
//For this we get all pizzas & prices from Menu screen and put them in drop down menu

const pizzas = document.getElementsByClassName("gourmet_pizza");
const pizzaChoice = document.getElementById('H1');
const pizzaChoicePrice = document.getElementById('sum');
const lineForAmend = document.getElementById('constructor_result');

var isOpen = false;
pizzaChoice.onmousedown = function () {
  if (!isOpen) {
    for (var i = 0; i < pizzas.length; i++) {
      const newrow = document.createElement('tr');
      newrow.classList.add('options');
      const newelement = document.createElement('td');
      newelement.classList.add('options');
      const newElementPrice = document.createElement('td');
      newElementPrice.classList.add('options');
      const blankTD1 = document.createElement('td');
      blankTD1.classList.add('options');
      const blankTD2 = document.createElement('td');
      blankTD2.classList.add('options');
      const blankTD3 = document.createElement('td');
      blankTD3.classList.add('options');
      blankTD3.innerHTML = '€';
      const pizza = pizzas[i].getElementsByClassName('pizza_about');
      const pizzatitle = pizza[0].firstChild.nextSibling;
      newElementPrice.innerHTML = pizza[0].nextSibling.nextSibling.innerText.substring(1);
      newelement.innerHTML = pizzatitle.innerHTML;
      lineForAmend.insertAdjacentElement('afterend', newrow);
      newrow.appendChild(newelement);
      newrow.appendChild(blankTD1);
      newrow.appendChild(blankTD2);
      newrow.appendChild(blankTD3);
      newrow.appendChild(newElementPrice);
      newelement.addEventListener('click', function () {
        pizzaChoice.innerHTML = newelement.innerHTML;
        pizzaChoicePrice.innerHTML = newElementPrice.innerHTML;
        clearIngridients();
        sum();
        if (isOpen) {
          const options = document.getElementsByClassName('options');
          for (var k = 0; k < options.length; k++) {
            options[k].classList.add('hidden');
          }
          isOpen = false;
        }
      })
    }
  }
  isOpen = true;
  sum();
};

//minus&plus in order
const plus = document.getElementById('plus');
const minus = document.getElementById('minus');
const amount = document.getElementById('amount');
plus.addEventListener('click', function () {
  amount.innerText = Number.parseInt(Number.parseInt(amount.innerText) + 1);
  sum();
});
minus.addEventListener('click', function () {
  if (amount.innerText = '0') {
    addedPrice = 0.0;
    clearIngridients();
    sum();
    return false;
  }
  amount.innerText = Number.parseInt(Number.parseInt(amount.innerText) - 1);
  sum();
})

//custom selection menu
let addedPrice = Number(0.0);

const customize = document.getElementById('customize');
const sizeSelect = document.getElementById('size');
const ingridients = document.getElementById('ingridients');
const cheese = document.getElementById('cheese');
const corn = document.getElementById('corn');
const salami = document.getElementById('salami');
const sizeMax = document.getElementById('sizeMax');
const sizeMid = document.getElementById('sizeMid');
const sizeMini = document.getElementById('sizeMini');

//drop down menu

customize.addEventListener('click', function () {
  sizeSelect.classList.toggle('hidden');
  ingridients.classList.toggle('hidden');
  cheese.classList.toggle('hidden');
  corn.classList.toggle('hidden');
  salami.classList.toggle('hidden');
})
//size selection (mid=default)
let sizeRate = Number(1.0);
sizeMax.addEventListener('click', function () {
  sizeMax.classList.add('selected');
  sizeMid.classList.remove('selected');
  sizeMini.classList.remove('selected');
  sizeRate = 1.5;
  clearIngridients();
  sum();
})
sizeMid.addEventListener('click', function () {
  sizeMid.classList.add('selected');
  sizeMax.classList.remove('selected');
  sizeMini.classList.remove('selected');
  sizeRate = 1.0;
  clearIngridients();
  sum();
})
sizeMini.addEventListener('click', function () {
  sizeMini.classList.add('selected');
  sizeMid.classList.remove('selected');
  sizeMax.classList.remove('selected');
  sizeRate = 0.75;
  clearIngridients();
  sum();
})
//adding ingridients
const addCheese = document.getElementById('addCheese');
let cheeseFlag = true;
addCheese.addEventListener('click', function () {
  cheese.classList.toggle('selected');
  if (cheeseFlag) {
    addedPrice = 1.5;
    addCheese.innerText = '-';
    cheeseFlag = false;
  } else {
    addedPrice = 0.0;
    addCheese.innerText = '+';
    cheeseFlag = true;
  };
  sum();
});

const addCorn = document.getElementById('addCorn');
let cornFlag = true;
addCorn.addEventListener('click', function () {
  corn.classList.toggle('selected');
  if (cornFlag) {
    addedPrice = 1.0;
    addCorn.innerText = '-';
    cornFlag = false;
  } else {
    addedPrice = 0.0;
    addCorn.innerText = '+'
    cornFlag = true;
  }
  sum();
});

const addSalami = document.getElementById('addSalami');
let salamiFlag = true;
addSalami.addEventListener('click', function () {
  salami.classList.toggle('selected');
  if (salamiFlag) {
    addedPrice = 2.0;
    addSalami.innerText = '-';
    salamiFlag = false;
  } else {
    addedPrice = 0.0;
    addSalami.innerText = '+';
    salamiFlag = true;
  }
  sum();
});
//to clear ingridients choice when pizza size is changed or new pizza selected
function clearIngridients() {
  salami.classList.remove('selected');
  corn.classList.remove('selected');
  cheese.classList.remove('selected');
  addCheese.innerText = "+";
  addCorn.innerText = "+";
  addSalami.innerText = '+';
  salamiFlag=true;
  cornFlag=true;
  cheeseFlag = true;
};

// sum calculator
function sum() {
  const amount = document.getElementById('amount');
  for (var l = 0; l < pizzas.length; l++) {
    if (pizzaChoice.innerText == pizzas[l].firstElementChild.firstElementChild.innerText) {
      var price = Number.parseFloat(pizzas[l].lastElementChild.innerText.substring(1)).toFixed(2);
      var currentAmount = Number.parseInt(amount.innerText);
      const totalSum = Number.parseFloat(price * currentAmount * sizeRate + addedPrice).toFixed(2);
      pizzaChoicePrice.innerText = totalSum;
      addedPrice = 0.0;
    }
  }
};

// bill calculations
const discounts = new Map([
  ['1234', 0.95], ['4321', 0.97], ['5555', 0.9], ['1111', 0.93], ['1111', 0.6]
]);

let discount = Number(1.0);
const tax = Number(0.2);//20%
const itemTotal = document.getElementById('itemTotal');
const discountDiv = document.getElementById('discount');
const taxes = document.getElementById('taxes');
const toPay = document.getElementById('toPay');
const coupon = document.getElementById('coupon');
var intervalId = window.setInterval(function () {
  if (discounts.get(coupon.value)) {
    discount = discounts.get(coupon.value);
    console.log(discount);
  }
  let temp = Number.parseFloat(pizzaChoicePrice.innerText) * discount;
  itemTotal.lastElementChild.innerText = Number.parseFloat(temp).toFixed(2);
  let tempDiscount = Number.parseFloat(pizzaChoicePrice.innerText).toFixed(2) - Number.parseFloat(itemTotal.lastElementChild.innerText).toFixed(2);
  discountDiv.lastElementChild.innerText = Number.parseFloat(tempDiscount).toFixed(2);
  let taxAmount = Number.parseFloat(temp).toFixed(2) * tax;
  taxes.lastElementChild.innerText = Number.parseFloat(taxAmount).toFixed(2);
  let finalPay = Number.parseFloat(temp).toFixed(2) + Number.parseFloat(taxAmount).toFixed(2);
  toPay.lastElementChild.innerText = Number.parseFloat(finalPay).toFixed(2);
}, 500);

//sweetalert
const menu_specials = document.getElementById('menu_specials');
menu_specials.addEventListener('click', function () {
  Swal.fire({
    position: 'top-end',
    icon: 'success',
    iconColor: "#FF5B00",
    title: 'You just received a coupon!',
    text: 'Scroll down to use it. 5% off!',
    color: '#7B3000',
    showConfirmButton: false,
    timer: 3500
  })
  coupon.value = '1234';
})
//nightlight toggle
const daynight = document.getElementById('daynight');
const screens = document.getElementsByClassName('screen');
daynight.addEventListener('click', function () {
  for (var i = 0; i < screens.length; i++) {
    screens[i].classList.toggle('screen_daylight');
    screens[i].classList.toggle('screen_nightlight');
  }
})